import flask
from flask import Response
from inception_blocks_v2 import *
from fr_utils import *

app = flask.Flask(__name__)

def init():
    global FRmodel, database
    K.set_image_data_format('channels_first')
    np.set_printoptions(threshold=np.nan)
    FRmodel = faceRecoModel(input_shape=(3, 96, 96))
    print("Total Params:", FRmodel.count_params())

    with tf.Session() as test:
        tf.set_random_seed(1)
        y_true = (None, None, None)
        y_pred = (tf.random_normal([3, 128], mean=6, stddev=0.1, seed=1),
                  tf.random_normal([3, 128], mean=1, stddev=1, seed=1),
                  tf.random_normal([3, 128], mean=3, stddev=4, seed=1))
        loss = triplet_loss(y_true, y_pred)

        print("loss = " + str(loss.eval()))

    FRmodel.compile(optimizer='adam', loss=triplet_loss, metrics=['accuracy'])
    load_weights_from_FaceNet(FRmodel)

def addOriginalImageToDatabase(userEmail, imageUrl):
    database[userEmail] = img_to_encoding(imageUrl, FRmodel)
    print('database: ', database)

def getParameters():
    parameters = []
    parameters.append(flask.request.args.get('email'))
    parameters.append(flask.request.args.get('path'))
    parameters.append(flask.request.args.get('current_image'))
    return parameters

def sendResponse(responseObj):
    response = Response(response=responseObj, status=200, mimetype='application/json')
    response.headers.add('Access-Control-Allow-Origin', '*')
    response.headers.add('Access-Control-Allow-Methods', 'GET')
    response.headers.add('Access-Control-Allow-Headers',
                         'accept,content-type,Origin,X-Requested-With,Content-Type,access_token,Accept,Authorization,'
                         'source')
    response.headers.add('Access-Control-Allow-Credentials', True)
    return response
# GRADED FUNCTION: triplet_loss

def triplet_loss(y_true, y_pred, alpha=0.2):
    """
    Implementation of the triplet loss as defined by formula (3)

    Arguments:
    y_true -- true labels, required when you define a loss in Keras, you don't need it in this function.
    y_pred -- python list containing three objects:
            anchor -- the encodings for the anchor images, of shape (None, 128)
            positive -- the encodings for the positive images, of shape (None, 128)
            negative -- the encodings for the negative images, of shape (None, 128)

    Returns:
    loss -- real number, value of the loss
    """

    anchor, positive, negative = y_pred[0], y_pred[1], y_pred[2]

    # Step 1: Compute the (encoding) distance between the anchor and the positive, you will need to sum over axis=-1
    pos_dist = tf.reduce_sum(tf.square(tf.subtract(anchor, positive)), axis=-1)
    # Step 2: Compute the (encoding) distance between the anchor and the negative, you will need to sum over axis=-1
    neg_dist = tf.reduce_sum(tf.square(tf.subtract(anchor, negative)), axis=-1)
    # Step 3: subtract the two previous distances and add alpha.
    basic_loss = pos_dist - neg_dist + alpha
    # Step 4: Take the maximum of basic_loss and 0.0. Sum over the training examples.
    loss = tf.reduce_sum(tf.maximum(basic_loss, 0.0))

    return loss

def verify(image_path, identity, database, model):
    """
    Function that verifies if the person on the "image_path" image is "identity".

    Arguments:
    image_path -- path to an image
    identity -- string, name of the person you'd like to verify the identity. Has to be a resident of the Happy house.
    database -- python dictionary mapping names of allowed people's names (strings) to their encodings (vectors).
    model -- your Inception model instance in Keras

    Returns:
    dist -- distance between the image_path and the image of "identity" in the database.
    door_open -- True, if the door should open. False otherwise.
    """



    # Step 1: Compute the encoding for the image
    encoding = img_to_encoding(image_path, model)

    # Step 2: Compute distance with identity's image
    dist = np.linalg.norm(database[identity] - encoding)

    # Step 3: Open the door if dist < 0.7, else don't open
    if dist < 0.7:
        print("It's " + str(identity) + ", welcome home!")
        door_open = True
    else:
        print("It's not " + str(identity) + ", please go away")
        door_open = False

    ### END CODE HERE ###

    return dist

@app.route("/predict", methods=["GET"])
def predict():
    email = flask.request.args.get('email')
    originalSelfie = flask.request.args.get("selfie")
    originalPath = flask.request.args.get('path')

    #get tokens for paths to images
    selfieToken = flask.request.args.get('token1')
    pathToken = flask.request.args.get('token2')

    #converting paths to proper
    selfie = returnConvertedPath(originalSelfie, 'images', selfieToken)
    path = returnConvertedPath(originalPath, 'requests', pathToken)

    addOriginalImageToDatabase(email, selfie)

    dist = verify(path, email, database=database, model=FRmodel)
    return sendResponse(str(dist))

def returnConvertedPath(path, imageType, token):
    prefix = 'https://firebasestorage.googleapis.com/v0/b/verify-me-40668.appspot.com/o/'+imageType
    suffix = path.split(prefix)[1]
    print(suffix)
    suffix = suffix.replace('@', '%40')
    print(suffix)
    suffix = suffix.replace('/', '%2F')

    print(suffix)

    path = prefix + suffix + '&token='+token
    return path

if __name__ == "__main__":
    print(("*Loading Keras model and Flask starting server..."))
    init()
    app.run(threaded=True)

